## Background
In safety and security work, we're used to enumerating threats and building mitigations upon that list. This matrix encourages a risk management approach where not only are risks enumerated but they are also assigned a gravity, tolerance level, and measures which ensure mitigation is happening.
## Sample Matrix 
This matrix is very basic and users are encouraged to amplify it based upon their needs or security posture. A key to successful use of a matrix like this is scheduling periodic reviews so that it is an iterative document just as safety and security are iterative.
| Risk/Asset | Risk Level/Impact | Risk Likelihood | Risk Tolerance/Appetite | Mitigaton Approach | Mitigation Party | Mitigation Validation |
| ------ | ----- | -----| ----- | -----| -----| ----- |  
| This lists the risk | This is the impact should the asset be compromised/risk not mitigated. | How likely is this risk? | How severe is the impact of this risk? | How will we mitigate this risk (e.g. transfer the risk via insurance; accept the risk as is; mitigate the risk via affirmative steps)? | Who is reponsible for ensuring mitigation is happening? | How and when do we evaluate the mitigation? |  

It's important to note that there will always be residual risk and that's natural. 

## Bonus Points  
* If you're using a spreadsheet application to manage this matrix, use separate tabs for physical, digital, finanacial, etc as your formation/posture would warrant.
* Build with other like groups (size, issue area/work, and political/ideological alignment) because sometimes tips around insurance, consultants, framing, etc can be shared for safety and security more broadly.
